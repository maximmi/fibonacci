import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class MainForm {

	MathCore core = new MathCore();
	
	private JFrame frmFibonacci;
	private JLabel lblFunction;
	private JLabel label;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frmFibonacci.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void init()
	{
		getLblFunction().setText("Function: " +core.getFunction());
		label = new JLabel("Min = "+core.getMin());
		label.setBounds(10, 36, 454, 14);
		frmFibonacci.getContentPane().add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 61, 454, 212);
		frmFibonacci.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setWrapStyleWord(true);
		for (String str : core.getLog())
		{
			getTextArea().append(str);
		}
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFibonacci = new JFrame();
		frmFibonacci.setTitle("Fibonacci optimization method");
		frmFibonacci.setResizable(false);
		frmFibonacci.setBounds(100, 100, 480, 312);
		frmFibonacci.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFibonacci.getContentPane().setLayout(null);
		
		lblFunction = new JLabel("Function: ");
		lblFunction.setBounds(10, 11, 454, 14);
		frmFibonacci.getContentPane().add(lblFunction);

		init();
	}
	public JLabel getLblFunction() {
		return lblFunction;
	}
	public JTextArea getTextArea() {
		return textArea;
	}
}
