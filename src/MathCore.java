import java.util.ArrayList;
import java.util.List;

class Pair
{
	double a;
	double b;
	public void swap() {
		double tmp;
		tmp=a;
		a=b;
		b=tmp;
	}
}

public class MathCore {
	private List<Double> fibs = new ArrayList<>(3);
	private List<String> __out = new ArrayList<>();
	private String function;
	double e = 0.001;
	double f(double x) 
	{
		//return (2*x*x - 12);
		return x*x+12*x-7+3*Math.pow(x, 4)-4*Math.pow(x, 3);
	}
	
	public String getFunction()
	{
		function = "2*x*x - 12";
		return function;
	}
	
	public Pair swann()
	{
		Pair tmp = new Pair();
		double start = Math.random();
		double t = Math.random();
		double x1 = start;
		double k = 0;
		double st0 = f(start-t);
		double st1 = f(t);
		double st2 = f(start+t);
		if (st0>=st1 && st1<=st2)
		{
			tmp.a = start-t;
			tmp.b = start+t;
			return tmp;
		}
		else if (st0<=st1 && st1>=st2)
		{
			return swann();
		}
		double d = 0;
		 if (st0>=st1 && st1>=st2)
		 {
			 d = t;
			 tmp.a = start;
			 x1 = start+t;
			 k++;
		 } else if (st0<=st1 && st1<=st2) 
		 {
			 d = -t;
			 tmp.b = start;
			 x1 = start-t;
			 k++;
		 }
		 double xk = 0;;
		 while (f(xk)>=f(x1))
		 {
			xk = xk+Math.pow(2, k) * d;
			if(f(xk)<f(x1) && d==t) tmp.a = xk;
			else if (f(xk)<f(x1) && d==-t) tmp.b = xk;
			k++;
			x1 = xk;
		 }
		 if (d==t) tmp.b=xk; else tmp.a=xk;
		 return tmp;
	}
	
	public Pair swann2()
	{
		Pair tmp = new Pair();
		double x0 = Math.random();
		double h = 0.01;
		double x=x0;
		int k=0;
		if (f(x0)<=f(x0+h)) h=-h;
		double xk=x0+h;
		while (f(xk)<f(x))
		{
			x = xk;
			k++;
			xk = x+Math.pow(2, k) * h;
		}
		tmp.a = x-(Math.pow(2, k-1) * h);
		tmp.b = xk;
		return tmp;
	}
	
	private void println(Object str)
	{
		__out.add(str.toString());
	}
	
	public double getMin()
	{
		Pair in = this.swann2();
		if (in.a > in.b) in.swap();
		return this.compute(in.a, in.b);
	}
	
	public List<String> getLog()
	{
		return __out;
	}
	
	
	public MathCore()
	{
		fibs.add(0.0);
		fibs.add(1.0);
	}
	
	public double fib(int n)
	{
		if(n<1) return 1;
		try
		{
			return fibs.get(n);
		}
		catch (IndexOutOfBoundsException e)
		{
			{
				for (int i = fibs.size(); i<=n; i++)
				{
					fibs.add(fibs.get(i-1)+fibs.get(i-2));
				}
			}	
			return fibs.get(n);
		}
	}
	
	public Double compute(double a, double b)
	{
		println("a="+a+", b="+b+"\n");
		double x1, x2, xf1, xf2;
		int k = 0;
		int n = 0;
		double f = (b - a) / e;
		while(fib(n) < f)
		{
			n++;
		}
		x1 = a + fib(n - 2) / fib(n) * (b - a);
		x2 = a + fib(n - 1) / fib(n) * (b - a);
		xf1 = f(x1);
		xf2 = f(x2);
		do
		{
			k++;
			if(xf1 >= xf2)
			{
				a = x1;
				x1 = x2;
				xf1 = xf2;
				x2 = a + fib(n - k - 1) / fib(n - k) * (b - a);
				xf2 = f(x2); 
			}
			else
			{
				b = x2;
				x2 = x1;
				xf2 = xf1;
				x1 = a + fib(n - k - 2) / fib(n - k) * (b - a);
				xf1 = f(x1);
			}
			println("�������� � "+k+'\n'+"x1 = "+x1+"\t\tF(x1) = "+xf1+"\nx2 = "+x2+"\t\tF(x2) = "+xf2+'\n');
		}
		while(Math.abs(b - a) > e && k<=n);
		return (a + b) / 2;
	}
	
}
